#! /usr/bin/perl -w

###############################################################
# Project: Perl Server Socket for multiperson chat
# Description: A convention way to implement multiperson chat
#					via select().
# Date: 2009.05.16
# Programmer: Lin Xin Yu
# Website:http://importcode.blogspot.com
###############################################################

use strict;
use warnings;
use IO::Socket;
use IO::Select;

#=============== Network Settings =============================
my $MAX_LEN = 1024;
my $host = 'localhost';
my $port = shift || 3456;

#==============================================================
#********** THIS IS THE MAIN PROGRAM **************************
#==============================================================

my $buf;

# Create the receiving socket
my $server = new IO::Socket::INET(	LocalPort	=>	$port,
												Proto			=>	'tcp',
												Listen		=>	SOMAXCONN,
												Reuse			=>	1,)
	or	die "Could not create socket: $!\n";

#=============== Select() Settings ============================

# create handle set for reading
my $readSet = new IO::Select();

# add server socket into readSet
$readSet->add($server);
$readSet->add(\*STDIN);
my %socketSet = ();

#=============== Main Process =================================

while (1) {

	# get a set of readable socket (blocks until at least one socket is ready)
	my ($readableSet) = IO::Select->select($readSet, undef, undef, 0);
	
	# take all readable socket in turn
	foreach my $socket (@$readableSet) {
		# accpeting new client if the socket is serverSocket
		if ($socket == $server) {
			my $client = $server->accept();
			$readSet->add($client);
			$socketSet{$client} = $client;
		}elsif($socket == \*STDIN){
			$buf = <STDIN>;
			if($buf =~ /^(exit|quit)/){
				foreach my $closeSocket(values %socketSet){
					syswrite($closeSocket, "Server is going to offline after 5 Seconds\n", $MAX_LEN);
				}
				
				sleep(5);
				foreach my $closeSocket(values %socketSet){
					close($closeSocket);
				}
				close($server);
				exit(0);
			}	
		}
		# otherwise it is an ordinary socket and we should read and process the request 
		else {
			sysread($socket, $buf, $MAX_LEN);
			if($buf) {
				if($buf =~ /^(:exit|:quit|:q)/i){
					$buf =~ s/^(:exit|:quit|:q)[ ]+//;
					delete $socketSet{$socket};
					$readSet->remove($socket);
					close($socket); 				
				}
				foreach my $closeSocket(values %socketSet){
					if($closeSocket != $socket){
						syswrite($closeSocket, $buf, $MAX_LEN);
					}			
				}			
			} else {	# the client has closed the socket or aborted
				# remove the socket from the $readSet and close it
				delete $socketSet{$socket};
				$readSet->remove($socket);
				close($socket);
			}
		}
	}
}

#==============================================================
#********** END OF THE MAIN PROGRAM ***************************
#==============================================================
