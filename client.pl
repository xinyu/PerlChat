#! /usr/bin/perl
###############################################################
# Project: Perl Client Socket for multiperson chat
# Description: A convention way to implement multiperson chat
#					via select().
# Date: 2009.05.16
# Programmer: Lin Xin Yu
# Website:http://importcode.blogspot.com
###############################################################

use strict;
use warnings;
use IO::Socket;
use IO::Select;

#=============== Network Settings =============================
my $MAX_LEN = 1024;
my $remote_host = shift || 'localhost';
my $remote_port = shift || 3456;
my $client;

#==============================================================
#********** THIS IS THE MAIN PROGRAM **************************
#==============================================================

# get use name from input
print "please enter your name:";
chomp(my $name = <STDIN>);
print STDERR $name.":";	

# Socket connection for Client.
$client = new IO::Socket::INET(	PeerAddr => $remote_host,
										PeerPort => $remote_port,
                          		Proto    => 'tcp',
                           	Type     => SOCK_STREAM,
                           	Timeout		=> 20)
    or die "Couldn't connect to $remote_host:$remote_port : $@\n";

my $buf;

#=============== Select() Settings ============================

# create handle set for reading
my $readSet = new IO::Select();

# add client socket into readSet
$readSet->add($client);
$readSet->add(\*STDIN);


while (1) {

	# get a set of readable socket (blocks until at least one socket is ready)
	my ($readableSet) = IO::Select->select($readSet, undef, undef, 0);
	
	# take all readable socket in turn
	foreach my $socket (@$readableSet) {
		if($socket == \*STDIN){
			chomp($buf = <STDIN>);
			if($buf =~ /^(:exit|:quit|:q)/){
				close($client);
				exit(0);		
			}else{
				syswrite($client, "$name:$buf\n", $MAX_LEN);
				print STDERR $name.":";	
			}	
		}
		# otherwise it is an ordinary socket and we should read and process the request 
		else {
			sysread($socket, $buf, $MAX_LEN);
			if($buf) {
				my $i=0;
				while($i++<256){
					print STDERR "\b";				
				}
				print STDERR $buf;
				print STDERR $name.":";
			} else {	# the server has close the socket or aborted
				# remove the socket from the $readSet and close it
				close($client);
				exit(0);
			}
		}
	}
}

#==============================================================
#********** END OF THE MAIN PROGRAM ***************************
#==============================================================

